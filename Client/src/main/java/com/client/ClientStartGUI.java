package com.client;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.rmi.AccessException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

import com.interf.assign2.Constants;
import com.interf.assign2.Remote_assign2;

import models.Car;

public class ClientStartGUI {

	private JFrame frame;
	private JLabel label1;
	private JTextField fldYear;
	private JTextField fldPrice;
	private JTextField fldCap;
	private JLabel lblYear;
	private JLabel lblPurchasePrice;
	private JLabel lblEngineCapacity;
	private JButton btnCalculateTax;
	private JButton btnCalculateReselingPrice;
	private JTextField textField;
	
	public ClientStartGUI() throws RemoteException, NotBoundException {
			frame = new JFrame("RMI Car Tax and Price Calculator");
			frame.setBounds(100, 100, 450, 401);
			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			frame.getContentPane().setLayout(null);
			
			label1=new JLabel("Welcome to RMI calculator for Cars!");
			label1.setFont(new Font("DejaVu Sans", Font.BOLD, 18));
			label1.setBounds(39, 26, 389, 31);
			frame.getContentPane().add(label1);
			
			fldYear = new JTextField();
			fldYear.setFont(new Font("Dialog", Font.PLAIN, 18));
			fldYear.setBounds(199, 62, 229, 41);
			frame.getContentPane().add(fldYear);
			fldYear.setColumns(10);
			
			fldPrice = new JTextField();
			fldPrice.setFont(new Font("Dialog", Font.PLAIN, 18));
			fldPrice.setBounds(199, 117, 229, 41);
			frame.getContentPane().add(fldPrice);
			fldPrice.setColumns(10);
			
			fldCap = new JTextField();
			fldCap.setFont(new Font("Dialog", Font.PLAIN, 18));
			fldCap.setBounds(199, 170, 229, 41);
			frame.getContentPane().add(fldCap);
			fldCap.setColumns(10);
			
			lblYear = new JLabel("Purchase Year:");
			lblYear.setFont(new Font("Dialog", Font.BOLD, 18));
			lblYear.setBounds(31, 75, 169, 15);
			frame.getContentPane().add(lblYear);
			
			lblPurchasePrice = new JLabel("Purchase Price:");
			lblPurchasePrice.setFont(new Font("Dialog", Font.BOLD, 18));
			lblPurchasePrice.setBounds(31, 131, 169, 15);
			frame.getContentPane().add(lblPurchasePrice);
			
			lblEngineCapacity = new JLabel("Engine Capacity:");
			lblEngineCapacity.setFont(new Font("Dialog", Font.BOLD, 18));
			lblEngineCapacity.setBounds(12, 183, 188, 28);
			frame.getContentPane().add(lblEngineCapacity);
			
		    btnCalculateTax = new JButton("Calculate TAX!");
			btnCalculateTax.setFont(new Font("Dialog", Font.BOLD, 16));
			btnCalculateTax.setBounds(22, 223, 178, 48);
			frame.getContentPane().add(btnCalculateTax);
			
			btnCalculateReselingPrice = new JButton("<html><b>Calculate</b><br>Reselling PRICE!</html>");
			btnCalculateReselingPrice.setFont(new Font("Dialog", Font.BOLD, 16));
			btnCalculateReselingPrice.setBounds(231, 223, 178, 48);
			frame.getContentPane().add(btnCalculateReselingPrice);
			
			textField = new JTextField();
			textField.setFont(new Font("Dialog", Font.PLAIN, 18));
			textField.setBounds(28, 301, 400, 41);
			frame.getContentPane().add(textField);
			textField.setColumns(10);
			textField.setEditable(false);
			
			frame.setVisible(true);
			
			/////////////////////////////////////////////////////////////////////////////////
		    /////////////////////////////////////////////////////////////////////////////////
			btnCalculateTax.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					double calculatedTax = 0;
					Registry registry = null;
					try {
						registry = LocateRegistry.getRegistry("localhost", Constants.RMI_PORT);
					} catch (RemoteException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					Remote_assign2 remote = null;
					try {
						remote = (Remote_assign2)registry.lookup(Constants.RMI_ID);
					} catch (AccessException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					} catch (RemoteException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					} catch (NotBoundException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					
					String strCarYear     = fldYear.getText();
					String strCarPrice    = fldPrice.getText();
					String strCarCapacity = fldCap.getText();
					Car testCar = new Car(Integer.parseInt(strCarYear), Integer.parseInt(strCarCapacity), 
							  Double.parseDouble(strCarPrice));
					try {
					  calculatedTax = remote.calculateTax(testCar);
					} catch (RemoteException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					//System.out.println(calculatedTax);
					textField.setText("TAX="+calculatedTax);
				}
			});
			/////////////////////////////////////////////////////////////////////////////////
			/////////////////////////////////////////////////////////////////////////////////
			
			btnCalculateReselingPrice.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					double calculatedResellingPrice = 0;
					Registry registry = null;
					try {
						registry = LocateRegistry.getRegistry("localhost", Constants.RMI_PORT);
					} catch (RemoteException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					Remote_assign2 remote = null;
					try {
						remote = (Remote_assign2)registry.lookup(Constants.RMI_ID);
					} catch (AccessException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					} catch (RemoteException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					} catch (NotBoundException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					
					String strCarYear     = fldYear.getText();
					String strCarPrice    = fldPrice.getText();
					String strCarCapacity = fldCap.getText();
					Car testCar = new Car(Integer.parseInt(strCarYear), Integer.parseInt(strCarCapacity), 
							  Double.parseDouble(strCarPrice));
					try {
						calculatedResellingPrice = remote.calculateSellingPrice(testCar);
					} catch (RemoteException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					//System.out.println(calculatedResellingPrice);
					if (calculatedResellingPrice>0)
					textField.setText("PRICE="+calculatedResellingPrice);
					else 
					textField.setText("CAR HAS NO VALUE!");
				}
			});
	}
	public static void main (String[] args) throws RemoteException, NotBoundException{
		ClientStartGUI g = new ClientStartGUI();
	}
}










