package com.client;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

import com.interf.assign2.Constants;
import com.interf.assign2.Remote_assign2;

import models.Car;

public class ClientStart {
	public static void main(String[] args) throws RemoteException, NotBoundException{
		Registry registry = LocateRegistry.getRegistry("localhost", Constants.RMI_PORT);
		Remote_assign2 remote = (Remote_assign2)registry.lookup(Constants.RMI_ID);
		System.out.println(remote.isLoginValid("test"));
		System.out.println(remote.calculateTax(new Car(2015, 1600, 25000.0)));
	}
}
