package com.server.assign2;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

import com.interf.assign2.Remote_assign2;

import models.Car;

public class RemoteImpl extends UnicastRemoteObject implements Remote_assign2 {

	protected RemoteImpl() throws RemoteException {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public boolean isLoginValid(String userName) throws RemoteException {
		if (userName.equals("user"))
			return true;
		else
		return false;
	}

	public double calculateTax(Car car) throws RemoteException {
        int sum;
		if(car.getEngineCapacity()<1600)
			sum = 8;
		else if (car.getEngineCapacity()<2000)
			sum = 18;
		else if (car.getEngineCapacity()<2600)
			sum = 72;
		else if (car.getEngineCapacity()<3000)
			sum = 144;
		else sum = 290;
		
		double tax = (car.getEngineCapacity()/200)
				* sum;
		
		return tax;
	}

	public double calculateSellingPrice(Car car) throws RemoteException {
		double sellingPrice = car.getPrice()-(car.getPrice()/7)*(2015-car.getFabricationYear());
		return sellingPrice;
	}

}
