package com.server.assign2;

import java.rmi.AlreadyBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

import com.interf.assign2.Constants;

public class RMIServerStart {
	public static void main(String[] args) throws RemoteException, AlreadyBoundException{
		RemoteImpl impl = new RemoteImpl();
	    Registry registry = LocateRegistry.createRegistry(Constants.RMI_PORT);
	    registry.bind(Constants.RMI_ID, impl);
	    System.out.println("SERVER STARTED!");
	}
}
