package com.interf.assign2;
import java.rmi.Remote;
import java.rmi.RemoteException;

import models.Car;
public interface Remote_assign2 extends Remote{
	public boolean isLoginValid(String userName) throws RemoteException;
	public double calculateTax(Car car) throws RemoteException;
	public double calculateSellingPrice(Car car) throws RemoteException;
}
