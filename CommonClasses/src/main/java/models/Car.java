package models;

import java.io.Serializable;

public class Car implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 5019298906527531819L;
	private int fabricationYear; 
	private int engineCapacity; 
	private double price;
	
	
	
	public Car(int fabricationYear, int engineCapacity, double price) {
		super();
		this.fabricationYear = fabricationYear;
		this.engineCapacity = engineCapacity;
		this.price = price;
	}
	public int getFabricationYear() {
		return fabricationYear;
	}
	public void setFabricationYear(int fabricationYear) {
		this.fabricationYear = fabricationYear;
	}
	public int getEngineCapacity() {
		return engineCapacity;
	}
	public void setEngineCapacity(int engineCapacity) {
		this.engineCapacity = engineCapacity;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + engineCapacity;
		result = prime * result + fabricationYear;
		long temp;
		temp = Double.doubleToLongBits(price);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Car other = (Car) obj;
		if (engineCapacity != other.engineCapacity)
			return false;
		if (fabricationYear != other.fabricationYear)
			return false;
		if (Double.doubleToLongBits(price) != Double.doubleToLongBits(other.price))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "Car [fabricationYear=" + fabricationYear + ", engineCapacity=" + engineCapacity + ", price=" + price
				+ "]";
	}
	
}
